# macOS Login Sound

Play a sound when logging in inside macOS!

## Instructions
1. Place the application inside `~/Library`. To do so, open _Go ► Go to Folder…_, and then type that directory path. 
2. Move the application into that directory.
3. Run the app, and kindly follow the on-screen instructions. 
4. Add the app as your login item through System Settings or System Preferences. 

## Hackin' Building
Feel free to make a fork of this repository to make the necessary changes. Contributions are welcome! 

To build, save the script as an application, and edit its `info.plist` file to set its `LSUIElement` value to `true`.  
