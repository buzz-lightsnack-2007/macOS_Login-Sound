FasdUAS 1.101.10   ��   ��    k             l     ��  ��      Login Sound     � 	 	    L o g i n   S o u n d   
  
 l     ��������  ��  ��        l     ��  ��    f ` The following function, convertPathToPOSIXString(thePath), is from Apple's documentation page.      �   �   T h e   f o l l o w i n g   f u n c t i o n ,   c o n v e r t P a t h T o P O S I X S t r i n g ( t h e P a t h ) ,   i s   f r o m   A p p l e ' s   d o c u m e n t a t i o n   p a g e .        i         I      �� ���� 40 convertpathtoposixstring convertPathToPOSIXString   ��  o      ���� 0 thepath thePath��  ��    k     %       O         Q         r         n     ! " ! 1    ��
�� 
ppth " 4    �� #
�� 
ditm # l  	  $���� $ c   	  % & % o   	 
���� 0 thepath thePath & m   
 ��
�� 
TEXT��  ��     o      ���� 0 thepath thePath  R      ������
�� .ascrerr ****      � ****��  ��    r     ' ( ' n     ) * ) 1    ��
�� 
ppth * o    ���� 0 thepath thePath ( o      ���� 0 thepath thePath  m      + +�                                                                                  sevs  alis    Z  MacBook SSD                ޿��BD ����System Events.app                                              ����޿��        ����  
 cu             CoreServices  0/:System:Library:CoreServices:System Events.app/  $  S y s t e m   E v e n t s . a p p    M a c B o o k   S S D  -System/Library/CoreServices/System Events.app   / ��     ,�� , L     % - - n     $ . / . 1   ! #��
�� 
psxp / o     !���� 0 thepath thePath��     0 1 0 l     ��������  ��  ��   1  2 3 2 i    4 5 4 I      �� 6���� 0 
play_sound   6  7�� 7 m      ��
�� 
snd ��  ��   5 k      8 8  9 : 9 l     �� ; <��   ; . ( The sound file must be in POSIX format.    < � = = P   T h e   s o u n d   f i l e   m u s t   b e   i n   P O S I X   f o r m a t . :  >�� > L      ? ? I    �� @��
�� .sysoexecTEXT���     TEXT @ l     A���� A b      B C B m      D D � E E  a f p l a y   - - d e b u g   C I    �������� 0 check_file_sound  ��  ��  ��  ��  ��  ��   3  F G F l     ��������  ��  ��   G  H I H i    J K J I      �������� 0 check_file_sound  ��  ��   K k     W L L  M N M r      O P O m      Q Q � R R   P o      ���� 0 sounds_startup   N  S T S r     U V U m    ��
�� boovfals V o      ���� 0 sounds_exist   T  W X W l   ��������  ��  ��   X  Y Z Y Q    T [ \ ] [ k     ^ ^  _ ` _ r     a b a m     c c � d d 8 ~ / L i b r a r y / S o u n d s / s t a r t u p . w a v b o      ���� 0 sounds_startup   `  e f e I   �� g��
�� .sysoexecTEXT���     TEXT g l    h���� h b     i j i m     k k � l l  c a t   j o    ���� 0 sounds_startup  ��  ��  ��   f  m�� m r     n o n m    ��
�� boovtrue o o      ���� 0 sounds_exist  ��   \ R      ������
�� .ascrerr ****      � ****��  ��   ] Q   " T p q r p k   % 4 s s  t u t r   % ( v w v m   % & x x � y y 8 ~ / L i b r a r y / S o u n d s / s t a r t u p . m p 3 w o      ���� 0 sounds_startup   u  z { z I  ) 0�� |��
�� .sysoexecTEXT���     TEXT | l  ) , }���� } b   ) , ~  ~ m   ) * � � � � �  c a t    o   * +���� 0 sounds_startup  ��  ��  ��   {  ��� � r   1 4 � � � m   1 2��
�� boovtrue � o      ���� 0 sounds_exist  ��   q R      ������
�� .ascrerr ****      � ****��  ��   r Q   < T � � � � k   ? J � �  � � � r   ? B � � � m   ? @ � � � � � 8 ~ / L i b r a r y / S o u n d s / s t a r t u p . m 4 a � o      ���� 0 sounds_startup   �  ��� � I  C J�� ���
�� .sysoexecTEXT���     TEXT � l  C F ����� � b   C F � � � m   C D � � � � �  c a t   � o   D E���� 0 sounds_startup  ��  ��  ��  ��   � R      ������
�� .ascrerr ****      � ****��  ��   � L   R T � � m   R S��
�� boovfals Z  � � � l  U U��������  ��  ��   �  ��� � L   U W � � o   U V���� 0 sounds_startup  ��   I  � � � l     ��������  ��  ��   �  � � � i    � � � I      �� ����� 0 check_folder_sounds   �  ��� � o      ���� 
0 create  ��  ��   � k     % � �  � � � Q     " � � � � k    
 � �  � � � I   �� ���
�� .sysoexecTEXT���     TEXT � l    ����� � m     � � � � � * f i l e   ~ / L i b r a r y / S o u n d s��  ��  ��   �  ��� � l  	 	�� � ���   � > 8 It should not error out when the Sounds folder exists.     � � � � p   I t   s h o u l d   n o t   e r r o r   o u t   w h e n   t h e   S o u n d s   f o l d e r   e x i s t s .  ��   � R      ������
�� .ascrerr ****      � ****��  ��   � Z    " � ��� � � =    � � � o    ���� 
0 create   � m    ��
�� boovtrue � I   �� ���
�� .sysoexecTEXT���     TEXT � l    ����� � m     � � � � � , m k d i r   ~ / L i b r a r y / S o u n d s��  ��  ��  ��   � L     " � � m     !��
�� boovfals �  � � � l  # #��������  ��  ��   �  ��� � L   # % � � m   # $��
�� boovtrue��   �  � � � l     ��������  ��  ��   �  � � � i    � � � I      ������� 0 window_configuration_ask  ��  �   � V     q � � � Q    l � � � � k   	 b � �  � � � r   	  � � � I  	 �~ � �
�~ .sysodisAaleR        TEXT � m   	 
 � � � � �  C o n f i g u r e ? � �} � �
�} 
mesS � m     � � � � � l T h e   s o u n d   f i l e   d o e s   n o t   e x i s t   o r   h a s   t h e   w r o n g   f o r m a t . � �| � �
�| 
btns � J     � �  � � � m     � � � � � %��   S t o p �  ��{ � m     � � � � � !�   C o n f i g u r e�{   � �z � �
�z 
dflt � m    �y�y  � �x ��w
�x 
cbtn � m    �v�v �w   � o      �u�u 0 response   �  � � � l   �t�s�r�t  �s  �r   �  ��q � Z    b � ��p � � =   ! � � � n     � � � 1    �o
�o 
bhit � o    �n�n 0 response   � m      � � � � � !�   C o n f i g u r e � Z   $ ] � ��m�l � =  $ + � � � I   $ )�k�j�i�k 0 window_configuration_setup  �j  �i   � m   ) *�h
�h boovtrue � k   . Y � �  � � � r   . B � � � I  . @�g � �
�g .sysodisAaleR        TEXT � m   . / � � � � � < T h e   a u d i o   h a s   b e e n   c o n f i g u r e d . � �f 
�f 
mesS  m   0 1 � � T o   m a k e   s u r e   t h a t   t h i s   p l a y s   d u r i n g   l o g   i n ,   p l e a s e   p l a c e   t h i s   a p p   a s   o n e   o f   y o u r   l o g i n   i t e m s . �e
�e 
btns J   2 8  m   2 3		 �

 "   D o n e �d m   3 6 � %�   P l a y�d   �c
�c 
dflt m   9 :�b�b  �a�`
�a 
cbtn m   ; <�_�_ �`   � o      �^�^ 0 response   �  Z   C V�]�\ =  C J n   C F 1   D F�[
�[ 
bhit o   C D�Z�Z 0 response   m   F I � %�   P l a y I   M R�Y�X�W�Y 0 runtime  �X  �W  �]  �\    l  W W�V�U�T�V  �U  �T   �S L   W Y m   W X�R
�R boovtrue�S  �m  �l  �p   � L   ` b m   ` a�Q
�Q boovfals�q   � R      �P�O�N
�P .ascrerr ****      � ****�O  �N   � L   j l   m   j k�M
�M boovfals � m    �L
�L boovtrue � !"! l     �K�J�I�K  �J  �I  " #$# i   %&% I      �H'�G�H 0 	file_copy  ' ()( o      �F�F 0 	inputfile 	inputFile) *�E* o      �D�D 0 
outputfile 
outputFile�E  �G  & k     ++ ,-, l     �C./�C  . 7 1 Input and output files must be in POSIX format.    / �00 b   I n p u t   a n d   o u t p u t   f i l e s   m u s t   b e   i n   P O S I X   f o r m a t .  - 1�B1 I    �A2�@
�A .sysoexecTEXT���     TEXT2 l    3�?�>3 b     454 b     676 b     898 m     :: �;;  c p   - v   - R  9 o    �=�= 0 	inputfile 	inputFile7 m    << �==   5 o    �<�< 0 
outputfile 
outputFile�?  �>  �@  �B  $ >?> l     �;�:�9�;  �:  �9  ? @A@ i   BCB I      �8D�7�8 0 	file_link  D EFE o      �6�6 0 	inputfile 	inputFileF G�5G o      �4�4 0 
outputfile 
outputFile�5  �7  C k     HH IJI l     �3KL�3  K 7 1 Input and output files must be in POSIX format.    L �MM b   I n p u t   a n d   o u t p u t   f i l e s   m u s t   b e   i n   P O S I X   f o r m a t .  J N�2N I    �1O�0
�1 .sysoexecTEXT���     TEXTO l    P�/�.P b     QRQ b     STS b     UVU m     WW �XX  l n   - s  V o    �-�- 0 	inputfile 	inputFileT m    YY �ZZ   R o    �,�, 0 
outputfile 
outputFile�/  �.  �0  �2  A [\[ l     �+�*�)�+  �*  �)  \ ]^] i   _`_ I      �(�'�&�( 0 window_configuration_setup  �'  �&  ` k    >aa bcb r     ded m     �%�% e o      �$�$ 
0 screen  c fgf l   �#�"�!�#  �"  �!  g hih V   <jkj Z   
7lmn� l =  
 opo o   
 �� 
0 screen  p m    �� m Q    9qrsq k    /tt uvu l   �wx�  w   User selection   x �yy    U s e r   s e l e c t i o nv z{z r    "|}| I    ��~
� .sysostdfalis    ��� null�  ~ ��
� 
prmp m    �� ��� < P l e a s e   s e l e c t   t h e   l o g i n   s o u n d .� ���
� 
ftyp� J    �� ��� m    �� ���  m p 3� ��� m    �� ���  w a v� ��� m    �� ���  m 4 a�  �  } o      �� 0 file_audio_original  { ��� r   # +��� I   # )���� 40 convertpathtoposixstring convertPathToPOSIXString� ��� o   $ %�� 0 file_audio_original  �  �  � o      �� 0 file_audio_original  � ��� r   , /��� m   , -�� � o      �� 
0 screen  �  r R      ���
� .ascrerr ****      � ****�  �  s L   7 9�� m   7 8�

�
 boovfalsn ��� =  < ?��� o   < =�	�	 
0 screen  � m   = >�� � ��� Q   B ����� k   E u�� ��� r   E b��� I  E `���
� .sysodisAaleR        TEXT� m   E F�� ��� * H o w   t o   u s e   t h i s   f i l e ?� ���
� 
mesS� m   G H�� ���. C o p y i n g   t h e   f i l e   i s   r e c o m m e n d e d .   H o w e v e r ,   i f   y o u   d o   n o t   p l a n   t o   m o v e   o r   e r a s e   t h e   f i l e ,   t h e n   y o u   m a y   u s e   t h e   l i n k i n g   o p t i o n ,   w h i c h   s a v e s   s t o r a g e   s p a c e .� ���
� 
btns� J   I R�� ��� m   I J�� ��� !�   C h a n g e� ��� m   J M�� ��� �=��   C o p y� ��� m   M P�� ��� �=݇   L i n k�  � ���
� 
dflt� m   U V�� � ��� 
� 
cbtn� m   Y Z���� �   � o      ���� 0 file_audio_action  � ��� r   c j��� n   c h��� 1   d h��
�� 
bhit� o   c d���� 0 file_audio_action  � o      ���� 0 file_audio_action  � ��� I   k q������� 0 check_folder_sounds  � ���� m   l m��
�� boovtrue��  ��  � ���� r   r u��� m   r s���� � o      ���� 
0 screen  ��  � R      ������
�� .ascrerr ****      � ****��  ��  � r   } ���� m   } ~���� � o      ���� 
0 screen  � ��� =  � ���� o   � ����� 
0 screen  � m   � ����� � ���� Q   �3���� k   � �� ��� Z   ������� =  � ���� o   � ����� 0 file_audio_action  � m   � ��� ��� �=��   C o p y� Z   � ������� D   � ���� o   � ����� 0 file_audio_original  � m   � ��� ���  w a v� I   � �������� 0 	file_copy  � ��� o   � ����� 0 file_audio_original  � ���� m   � ��� ��� 8 ~ / L i b r a r y / S o u n d s / s t a r t u p . w a v��  ��  � ��� D   � ���� o   � ����� 0 file_audio_original  � m   � ��� ���  m p 3� ��� I   � �������� 0 	file_copy  � ��� o   � ����� 0 file_audio_original  � ���� m   � ��� ��� 8 ~ / L i b r a r y / S o u n d s / s t a r t u p . m p 3��  ��  � ��� D   � �� � o   � ����� 0 file_audio_original    m   � � �  m 4 a� �� I   � ������� 0 	file_copy    o   � ����� 0 file_audio_original   �� m   � � �		 8 ~ / L i b r a r y / S o u n d s / s t a r t u p . m 4 a��  ��  ��  ��  � 

 =  � � o   � ����� 0 file_audio_action   m   � � � �=݇   L i n k �� Z   ��� D   � � o   � ����� 0 file_audio_original   m   � � �  w a v I   � ������� 0 	file_link    o   � ����� 0 file_audio_original   �� m   � � � 8 ~ / L i b r a r y / S o u n d s / s t a r t u p . w a v��  ��    D   � � !  o   � ����� 0 file_audio_original  ! m   � �"" �##  m p 3 $%$ I   ���&���� 0 	file_link  & '(' o   � ����� 0 file_audio_original  ( )��) m   � �** �++ 8 ~ / L i b r a r y / S o u n d s / s t a r t u p . m p 3��  ��  % ,-, D  	./. o  ���� 0 file_audio_original  / m  00 �11  m 4 a- 2��2 I  ��3���� 0 	file_link  3 454 o  ���� 0 file_audio_original  5 6��6 m  77 �88 8 ~ / L i b r a r y / S o u n d s / s t a r t u p . m 4 a��  ��  ��  ��  ��  ��  � 9:9 l ��������  ��  ��  : ;��; L   << m  ��
�� boovtrue��  � R      ������
�� .ascrerr ****      � ****��  ��  � k  (3== >?> l ((��@A��  @ ? 9 Was there previously a file? Let's clean that up anyway.   A �BB r   W a s   t h e r e   p r e v i o u s l y   a   f i l e ?   L e t ' s   c l e a n   t h a t   u p   a n y w a y .? CDC I (/��E��
�� .sysoexecTEXT���     TEXTE l (+F����F m  (+GG �HH � r m   - r f   ~ / L i b r a r y / S o u n d s / s t a r t u p . w a v   ~ / L i b r a r y / S o u n d s / s t a r t u p . m p 3   ~ / L i b r a r y / S o u n d s / s t a r t u p . m 4 a��  ��  ��  D I��I r  03JKJ m  01���� K o      ���� 
0 screen  ��  ��  �   k m    	��
�� boovtruei L��L l ==��������  ��  ��  ��  ^ MNM l     ��������  ��  ��  N OPO l     ��������  ��  ��  P QRQ i    #STS I      �������� 0 runtime  ��  ��  T Z     GUVW��U =    XYX I     �������� 0 check_file_sound  ��  ��  Y m    ��
�� boovfalsV I   
 �������� 0 window_configuration_ask  ��  ��  W Z[Z F    &\]\ =   ^_^ I    ��`���� 0 check_folder_sounds  ` a��a m    ��
�� boovfals��  ��  _ m    ��
�� boovtrue] >   $bcb I    "�������� 0 check_file_sound  ��  ��  c m   " #��
�� boovfals[ d��d Q   ) Cefge I   , 6��h���� 0 
play_sound  h i��i I   - 2�������� 0 check_file_sound  ��  ��  ��  ��  f R      ������
�� .ascrerr ****      � ****��  ��  g k   > Cjj klk l  > >��mn��  m � � An error would usually occur here when the file format is wrong, especially when WAV files are being played as if they were MP3 files.     n �oo   A n   e r r o r   w o u l d   u s u a l l y   o c c u r   h e r e   w h e n   t h e   f i l e   f o r m a t   i s   w r o n g ,   e s p e c i a l l y   w h e n   W A V   f i l e s   a r e   b e i n g   p l a y e d   a s   i f   t h e y   w e r e   M P 3   f i l e s .    l p��p I   > C�������� 0 window_configuration_ask  ��  ��  ��  ��  ��  R qrq l     ��������  ��  ��  r s��s l    t����t I     ������� 0 runtime  ��  �  ��  ��  ��       �~uvwxyz{|}~�~  u 
�}�|�{�z�y�x�w�v�u�t�} 40 convertpathtoposixstring convertPathToPOSIXString�| 0 
play_sound  �{ 0 check_file_sound  �z 0 check_folder_sounds  �y 0 window_configuration_ask  �x 0 	file_copy  �w 0 	file_link  �v 0 window_configuration_setup  �u 0 runtime  
�t .aevtoappnull  �   � ****v �s �r�q���p�s 40 convertpathtoposixstring convertPathToPOSIXString�r �o��o �  �n�n 0 thepath thePath�q  � �m�m 0 thepath thePath�  +�l�k�j�i�h�g
�l 
ditm
�k 
TEXT
�j 
ppth�i  �h  
�g 
psxp�p &�  *��&/�,E�W X  ��,E�UO��,Ew �f 5�e�d���c�f 0 
play_sound  �e �b��b �  �a
�a 
snd �d  �  �  D�`�_�` 0 check_file_sound  
�_ .sysoexecTEXT���     TEXT�c �*j+ %j x �^ K�]�\���[�^ 0 check_file_sound  �]  �\  � �Z�Y�Z 0 sounds_startup  �Y 0 sounds_exist  � 
 Q c k�X�W�V x � � �
�X .sysoexecTEXT���     TEXT�W  �V  �[ X�E�OfE�O �E�O�%j OeE�W 9X   �E�O�%j OeE�W X   �E�O�%j W 	X  fO�y �U ��T�S���R�U 0 check_folder_sounds  �T �Q��Q �  �P�P 
0 create  �S  � �O�O 
0 create  �  ��N�M�L �
�N .sysoexecTEXT���     TEXT�M  �L  �R & �j OPW X  �e  
�j Y fOez �K ��J�I���H�K 0 window_configuration_ask  �J  �I  � �G�G 0 response  �  ��F ��E � ��D�C�B�A�@ ��? �	�>�=�<
�F 
mesS
�E 
btns
�D 
dflt
�C 
cbtn�B 
�A .sysodisAaleR        TEXT
�@ 
bhit�? 0 window_configuration_setup  �> 0 runtime  �=  �<  �H r phe ^������lv�l�k� 	E�O��,�  >*j+ e  0�����a lv�l�k� 	E�O��,a   
*j+ Y hOeY hY fW 	X  f[OY��{ �;&�:�9���8�; 0 	file_copy  �: �7��7 �  �6�5�6 0 	inputfile 	inputFile�5 0 
outputfile 
outputFile�9  � �4�3�4 0 	inputfile 	inputFile�3 0 
outputfile 
outputFile� :<�2
�2 .sysoexecTEXT���     TEXT�8 �%�%�%j | �1C�0�/���.�1 0 	file_link  �0 �-��- �  �,�+�, 0 	inputfile 	inputFile�+ 0 
outputfile 
outputFile�/  � �*�)�* 0 	inputfile 	inputFile�) 0 
outputfile 
outputFile� WY�(
�( .sysoexecTEXT���     TEXT�. �%�%�%j } �'`�&�%���$�' 0 window_configuration_setup  �&  �%  � �#�"�!�# 
0 screen  �" 0 file_audio_original  �! 0 file_audio_action  � *� ������������������������������"*07G�
�  
prmp
� 
ftyp� 
� .sysostdfalis    ��� null� 40 convertpathtoposixstring convertPathToPOSIXString�  �  
� 
mesS
� 
btns
� 
dflt
� 
cbtn� 
� .sysodisAaleR        TEXT
� 
bhit� 0 check_folder_sounds  � 0 	file_copy  � 0 	file_link  
� .sysoexecTEXT���     TEXT�$?kE�O7he�k  . !*������mv� E�O*�k+ E�OlE�W 	X 	 
fY ��l  C 5�����a a mva la ka  E�O�a ,E�O*ek+ OmE�W 
X 	 
kE�Y ��m  � ��a   B�a  *�a l+ Y +�a  *�a l+ Y �a  *�a l+ Y hY K�a    B�a ! *�a "l+ #Y +�a $ *�a %l+ #Y �a & *�a 'l+ #Y hY hOeW X 	 
a (j )OlE�Y h[OY��OP~ �T������ 0 runtime  �  �  �  � �
�	������
 0 check_file_sound  �	 0 window_configuration_ask  � 0 check_folder_sounds  
� 
bool� 0 
play_sound  �  �  � H*j+  f  
*j+ Y 7*fk+ e 	 *j+  f�&  **j+  k+ W X  *j+ Y h ������� 
� .aevtoappnull  �   � ****� k     �� s����  �  �  �  � ���� 0 runtime  �  *j+   ascr  ��ޭ